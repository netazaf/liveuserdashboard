import React from 'react';
import { Redirect } from "react-router-dom"; 
import './RegistrationForm.css';

export class RegistrationForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {name: '',email:'','redirect': false};
  
      this.handleNameChange = this.handleNameChange.bind(this);
      this.handleEmailChange = this.handleEmailChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleNameChange(event) {
      this.setState({name: event.target.value});
    }

    handleEmailChange(event) {
      this.setState({email: event.target.value});
    }
  
    handleSubmit(event) {
      event.preventDefault();
      let userObj = {name:this.state.name,email:this.state.email}
      this.submitUser(userObj)
    }

    submitUser(userObj){
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ name: userObj.name, email: userObj.email })
      };
      fetch('http://localhost/php/register', requestOptions)
        .then(response => response.json())
        .then(data => {
          this.props.setCurrentUser(data[userObj.email])
          this.props.setOnlineUsers(data)
          this.setState({'redirect': true});
        });
    }
  
    render() {

      if(this.state.redirect){
        return <Redirect to='/dashboard'/>
      }

      return (
        <div className="registrationForm">
          <h1>Dashboard Log In</h1>
          <form onSubmit={this.handleSubmit}>
            <label>
              <input placeholder="Name" name="userName" required type="text" value={this.state.name} onChange={this.handleNameChange} /><br/>
              <input placeholder="Email" name="userEmail" required type="email" value={this.state.email} onChange={this.handleEmailChange} /><br/>
            </label>
            <input type="submit" value="LOGIN" />
          </form>
        </div>
      );
    }
  }