import React from 'react';
import UserData from './UserData/UserData'
import Popup from './UserData/Popup/popup';
import './Dashboard.css';

export class Dashboard extends React.Component {

    constructor(props) {
      super(props);
    }
  
    state = {showPopup:false};

    popupUser = (email) =>{
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({email: email})
          };
        fetch('http://localhost/php/get-user-details', requestOptions)
            .then(response => response.json())
            .then(data => {
                this.setSelectedUser(data);
                this.setPopup(true);
            });
    }

    setSelectedUser = (userData) =>{
        this.setState({userData: userData})
    }

    setPopup = (showPopup) =>{
        this.setState({showPopup: showPopup})
    }

    getAllUsers = () =>{
        return Object.values(this.props.onlineUsers)
    }

    getOnlineUsers = (email) =>{
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({email: email})
          };
        fetch('http://localhost/php/get-online-users', requestOptions)
            .then(response => response.json())
            .then(data => {
                this.setState({onlineUsers:data});
            });
    }

    async componentDidMount() {
        try {
          setInterval(async () => {
            this.getOnlineUsers(this.props.currentUser.name)
          }, 3000);
        } catch(e) {
          console.log(e);
        }
    }    

    render() {
      return (
        <div className="dashboard">
            <h1>Online Users Dashboard</h1>
            <div className="welcome">Welcome {this.props.currentUser.name}!</div>
            <div className="dashTable">
                <div className="dashTableHeads">
                    <span>Name</span>
                    <span>Entrance time</span>
                    <span>Last update time</span>
                    <span>User Ip</span>
                </div>
                {this.getAllUsers().map((user,index) => {
                    return(
                        <UserData 
                            name={user.name}
                            entranceTime={user.entranceTime}
                            lastUpdate={user.lastUpdate}
                            userIp={user.userIp}
                            handleClick={() => this.popupUser(user.email)}
                            email={user.email}
                            key={index}
                        />
                    )
                })
                }
            </div>
            {this.state.showPopup && 
                <Popup 
                    popupUser={this.state.userData} 
                    closePopup={() => this.setPopup(false)}
                />}
        </div>
      );
    }
  }