import React from 'react';
import './popup.css';

const Popup = ({popupUser,closePopup})=> {
    
    return (
        <div className="popup">
            <div className="close" onClick={closePopup}>X</div>
            <span className="popupLabel">Name: </span><span className="popupVal">{popupUser.name}</span><br/>
            <span className="popupLabel">Email: </span><span className="popupVal">{popupUser.email}</span><br/>
            <span className="popupLabel">User Agent: </span><span className="popupVal">{popupUser.userAgent}</span><br/>
            <span className="popupLabel">Entrance Time: </span><span className="popupVal">{popupUser.entranceTime}</span><br/>
            <span className="popupLabel">Visits Count: </span><span className="popupVal">{popupUser.visits}</span>
        </div>
    );
}

export default Popup;

