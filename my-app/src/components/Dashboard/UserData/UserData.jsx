import React from 'react';

const UserData = ({name,entranceTime,lastUpdate,userIp,handleClick,email})=> {
    return (
        <div className="userData" onClick={()=>handleClick(email)}>
            <span>{name}</span>
            <span>{entranceTime}</span>
            <span>{lastUpdate}</span>
            <span>{userIp}</span>
        </div>
    );
}

export default UserData;


