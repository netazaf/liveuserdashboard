import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { RegistrationForm } from './components/RegistrationForm/RegistrationForm'
import { Dashboard } from './components/Dashboard/Dashboard';

export class App extends React.Component {

  state = {
    currentUser: {},
    onlineUsers: {},
    showPopup: false
  }

  setCurrentUser = (currentUser) => {
    this.setState({currentUser: currentUser});
  }

  setOnlineUsers = (onlineUsers) => {
    this.setState({onlineUsers: onlineUsers});
  }

  componentWillUnmount(){
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ name: this.state.currentUser.name, email: this.state.currentUser.email })
    };

    fetch('http://localhost/php/set-user-offline', requestOptions)
        .then(response => response.json())
  }

  render() {
    return (
      <Router>
        <Switch>
          <Route exact path='/'>
            <RegistrationForm 
              setOnlineUsers={this.setOnlineUsers} 
              setCurrentUser={this.setCurrentUser}
            />
          </Route>
          <Route path='/dashboard'>
            <Dashboard 
              currentUser={this.state.currentUser}
              onlineUsers={this.state.onlineUsers}
            />
          </Route>
        </Switch>
      </Router>
    );
  }
}

export default App;