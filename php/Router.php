<?php
require 'App/Dashboard.php';

class Router{

    private $request;

    /**
     * @param $request
     */
    public function __construct($request){
        $this->request = $request;
    }

    /**
     * @param $route
     */
    public function get($route){
        $uri = trim( $this->request, "/" );
        $uri = explode("/", $uri);

        if($uri[0] == trim($route, "/")){
            $post = file_get_contents('php://input');
            $postArr = json_decode($post ,1);

            return $this->handleFunction($uri[0],$postArr);
        }
    }

    /**
     * @param $route
     */
    private function handleFunction($route, $postArr){
        $dashboard = new Dashboard($postArr);

        if($route == 'register'){
            return $dashboard->register();
        }elseif ($route == 'get-user-details'){
            return $dashboard->getUserDetails();
        }elseif($route == 'get-online-users'){
            return $dashboard->getOnlineUsers();
        }elseif ($route == 'set-user-offline'){
            return $dashboard->setUserOffline();
        }

    }

}