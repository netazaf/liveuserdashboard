<?php

require 'User.php';

class Dashboard
{
    private $postArr;

    /**
     * @param $postArr
     */
    public function __construct($postArr){
        $this->postArr = $postArr;
    }

    public function register(){
        $post = $this->postArr;
        $name = $post['name'];
        $email = $post['email'];
        if($name =='' || $email ==''){
            return 'error';
        }

        if(User::isUserExists($post['email'])){
            User::updateUser($name, $email);
        }else{
            User::createUser($name, $email);
        }

        return User::getOnlineUsers();
    }

    public function getUserDetails(){
        $post = $this->postArr;
        $email = $post['email'];
        if($email ==''){
            return 'error';
        }

        return User::getUserDetails($email);
    }

    public function getOnlineUsers(){
        $post = $this->postArr;
        $email = $post['email'];
        if($email ==''){
            return 'error';
        }

        User::updateUserLastUpdate($email);
        return User::getOnlineUsers();
    }

    public function setUserOffline(){
        $post = $this->postArr;
        $email = $post['email'];
        if($email ==''){
            return 'error';
        }
        return User::setUserOffline($email);
    }
}