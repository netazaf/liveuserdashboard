<?php

use \Datetime;

class User
{

    private static function getUserIp(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    private static function getUserAgent(){
        return $_SERVER['HTTP_USER_AGENT'] ?? null;
    }

    public static function isUserExists($email): bool
    {
        $usersArr = User::getJsonAsArr();
        return isset($usersArr[$email]);
    }

    public static function createUser($name, $email){
        $usersArr = User::getJsonAsArr();
        $arrCount = is_array($usersArr) ? count($usersArr) : 0;
        $arrKey = $email;
        $nowDate = User::getNowDate();
        $newUser = [
            $arrKey => [
                'name' => $name,
                'userIp' => User::getUserIp(),
                'userAgent' => User::getUserAgent(),
                'entranceTime' => $nowDate,
                'visitsCount' => 1,
                'lastUpdate' => $nowDate,
                'status' => 'online'
            ]
        ];

        $newJson = $arrCount > 0 ? array_merge($usersArr, $newUser) : $newUser;
        User::updateJsonData($newJson);
    }

    public static function setUserOffline($email){
        $usersArr = User::getJsonAsArr();
        $user = $usersArr[$email];
        $user['status'] = 'offline';
        User::updateJsonData($usersArr);
    }

    public static function updateUserLastUpdate($email){
        $usersArr = User::getJsonAsArr();
        $nowDate = User::getNowDate();
        $user = $usersArr[$email];
        $user['lastUpdate'] = $nowDate;
        User::updateJsonData($usersArr);
    }

    public static function getUserDetails($email)
    {
        $usersArr = User::getJsonAsArr();
        $user = $usersArr[$email];

        echo json_encode([
            'name' => $user['name'],
            'email' => $email,
            'userAgent' => $user['userAgent'],
            'entranceTime' => $user['entranceTime'],
            'visits' => $user['visitsCount'],
        ]);
    }

    private static function getNowDate(): string
    {
        $now = new DateTime();
        return $now->format('d-m-Y H:i:s');
    }

    public static function updateUser($name,$email){
        $usersArr = User::getJsonAsArr();
        $user = $usersArr[$email];
        $nowDate = User::getNowDate();
        $usersArr[$email]['name'] = $name !== $user['name'] ? $name : $user['name'];
        $usersArr[$email]['userIp'] = User::getUserIp();
        $usersArr[$email]['userAgent'] = User::getUserAgent();
        $usersArr[$email]['entranceTime'] = $nowDate;
        $usersArr[$email]['visitsCount'] = (intval($user['visitsCount']) + 1);
        $usersArr[$email]['lastUpdate'] = $nowDate;
        $usersArr[$email]['status'] = 'online';
        User::updateJsonData($usersArr);
    }

    public static function getOnlineUsers(){
        $usersArr = User::getJsonAsArr();
        $threeSecAgo = (time() - 3);

        $onlineUsers = [];
        foreach ($usersArr as $key=>$val){
            if(strtotime($val['lastUpdate']) >= $threeSecAgo && $val['status'] == 'online'){
                $onlineUsers[$key]['name'] = $val['name'];
                $onlineUsers[$key]['email'] = $key;
                $onlineUsers[$key]['entranceTime'] = $val['entranceTime'];
                $onlineUsers[$key]['lastUpdate'] = $val['lastUpdate'];
                $onlineUsers[$key]['userIp'] = $val['userIp'];
            }
        }
        echo json_encode($onlineUsers);
    }

    private static function getJsonAsArr(){
        $url = __DIR__.'/Users.json';
        $json = file_get_contents($url);
        return json_decode($json,1);
    }

    private static function updateJsonData($data){
        $url = __DIR__.'/Users.json';
        $jsonUser = json_encode($data);
        return file_put_contents($url,$jsonUser);
    }
}