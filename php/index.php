<?php

include 'Router.php';

$request = $_SERVER['REQUEST_URI'];
$router = new Router($request);

$router->get('/register');
$router->get('/get-user-details');
$router->get('/set-user-offline');
$router->get('/get-online-users');